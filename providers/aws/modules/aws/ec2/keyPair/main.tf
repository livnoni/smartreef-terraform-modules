# Creates an EC2 Key Pair whose name is in the format `<group name>-<name>`, using the specified public key file

# Resources are "grouped" by function by prefixing the key pair name with this param (e.g. website1)
variable "groupName" {}

# Name of the resource (e.g. mongodb)
variable "name" {}

# Path on the local file system to the public key file used to generate the key pair
variable "publicKeyPath" {}

# Key pair resource (e.g. website1-mongodb)
resource "aws_key_pair" "ec2-keypair" {
  key_name   = "${var.groupName}-${var.name}"
  public_key = "${file("${var.publicKeyPath}")}"
}

output "keyName" {
  value = "${aws_key_pair.ec2-keypair.key_name}"
}
