# Creates a VPC that allows all resources access to the Internet Gateway

variable "name" {}
variable "cidrBlock" {}
variable "tag-projectGroup" {}
variable "tag-project" {}

# VPC
resource "aws_vpc" "vpc" {
  cidr_block                     = "${var.cidrBlock}"
  instance_tenancy               = "default"
  enable_dns_support             = true
  enable_dns_hostnames           = true
  enable_classiclink_dns_support = false

  tags = {
    "Name"           = "${var.name}"
    "x:projectgroup" = "${var.tag-projectGroup}"
    "x:project"      = "${var.tag-project}"
  }
}

# Allow resources in the VPC internet access
resource "aws_internet_gateway" "vpc-internet-access" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"           = "vpc-${var.name}-internet-access"
    "x:projectgroup" = "${var.tag-projectGroup}"
    "x:project"      = "${var.tag-project}"
  }
}

# All traffic in the VPC can access the internet gateway
resource "aws_route_table" "vpc-all-traffic-access-igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.vpc-internet-access.id}"
  }

  tags = {
    "Name"           = "allow-all-resources-access-to-igw"
    "x:projectgroup" = "${var.tag-projectGroup}"
    "x:project"      = "${var.tag-project}"
  }
}

# Expose VPC ID
output "id" {
  value       = "${aws_vpc.vpc.id}"
  description = "ID of the VPC resource created"
}

# Expose Route Table ID
output "rt-all-traffic-access-to-igw" {
  value       = "${aws_route_table.vpc-all-traffic-access-igw.id}"
  description = "ID of the route table allowing all resources to access the Internet Gateway"
}
