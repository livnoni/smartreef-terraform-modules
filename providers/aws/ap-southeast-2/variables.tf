variable "awsRegion" {}
variable "awsAccount" {}
variable "awsProfile" {}

variable "AWSLambdaVPCAccessExecutionRoleArn" {
  default = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}
