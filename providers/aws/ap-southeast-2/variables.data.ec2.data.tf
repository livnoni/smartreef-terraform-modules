# Group name for data resources
variable groupName-data {
  default = "data"
}

variable "ec2-data-mongodb-tag-projectGroup" {
  default = "data"
}

variable "ec2-data-mongodb-tag-project" {
  default = ""
}
