# key pair used for a MongoDB instance
module "ec2-keyPair-data-mongodb" {
  source = "../modules/aws/ec2/keyPair"

  groupName     = "${var.groupName-data}"
  name          = "mongodb"
  publicKeyPath = "${var.ec2KeyPairDataMongoDbPublicKeyPath}"
}

# key pair used for a mySQL instance
module "ec2-keyPair-data-mysql" {
  source = "../modules/aws/ec2/keyPair"

  groupName     = "${var.groupName-data}"
  name          = "mysql"
  publicKeyPath = "${var.ec2KeyPairDataMySqlPublicKeyPath}"
}
