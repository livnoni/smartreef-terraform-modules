# Creates a security group to allow ICMP traffic

# Add the security group to the VPC
variable vpcId {}

# Tags to include on resources
variable "tags" {
  type = "map"
}

# Allow ICMP traffic
resource "aws_security_group" "icmp" {
  name        = "ICMP"
  description = "Allow pings"
  vpc_id      = "${var.vpcId}"

  ingress {
    description = "Allow ping"
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow ping reply"
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${var.tags}"
}

output "sgId" {
  value = "${aws_security_group.icmp.id}"
}
