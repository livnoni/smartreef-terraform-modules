#!/bin/bash
clear

# env vars
export TF_VAR_awsProfile=""
export TF_VAR_awsAccount=""
export TF_VAR_awsRegion="us-east-1"

# apply with the appropriate state
terraform destroy -state=state/sandbox/terraform.tfstate
