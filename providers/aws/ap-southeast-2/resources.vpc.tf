# Creates a VPC with subnets dedicated to resource types - databases, web servers, etc. An Internet Gateway allows resources to access the Internet.

# Main VPC
module "vpc" {
  source = "../modules/aws/vpc/igw/allAccess"

  name             = "main"
  cidrBlock        = ""
  tag-projectGroup = "${var.vpc-network-tag-projectGroup}"
  tag-project      = "${var.vpc-network-tag-project}"
}

# Add a subnet for data resources in the ap-southeast-2 region
module "vpc-subnet-data" {
  source = "../modules/aws/vpc/subnet"

  vpcId                  = "${module.vpc.id}"
  subnetAvailabilityZone = "${var.vpc-data-availability-zone}"
  cidrBlock              = ""

  routeTableId = "${module.vpc.rt-all-traffic-access-to-igw}"

  tags = {
    "Name"           = "${var.groupName-data}"
    "x:projectgroup" = "${var.vpc-network-tag-projectGroup}"
    "x:project"      = "${var.vpc-network-tag-project}"
  }
}

# Add a subnet for web resources in the ap-southeast-2 region
module "vpc-subnet-web" {
  source = "../modules/aws/vpc/subnet"

  vpcId                  = "${module.vpc.id}"
  subnetAvailabilityZone = "${var.vpc-data-availability-zone}"
  cidrBlock              = ""

  routeTableId = "${module.vpc.rt-all-traffic-access-to-igw}"

  tags = {
    "Name"           = "${var.groupName-web}"
    "x:projectgroup" = "${var.vpc-network-tag-projectGroup}"
    "x:project"      = "${var.vpc-network-tag-project}"
  }
}

# Add security groups to be used by instances in the VPC
module "vpc-security-group-icmp" {
  source = "../modules/aws/vpc/securityGroup/icmp"

  vpcId = "${module.vpc.id}"

  tags = {
    "Name"           = "icmp"
    "x:projectgroup" = "${var.vpc-network-tag-projectGroup}"
    "x:project"      = "${var.vpc-network-tag-project}"
  }
}

module "vpc-security-group-mongodb" {
  source = "../modules/aws/vpc/securityGroup/mongodb"

  vpcId = "${module.vpc.id}"

  tags = {
    "Name"           = "mongodb"
    "x:projectgroup" = "${var.vpc-network-tag-projectGroup}"
    "x:project"      = "${var.vpc-network-tag-project}"
  }
}
