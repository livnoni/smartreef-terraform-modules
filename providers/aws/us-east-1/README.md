# /providers/aws/us-east-1

AWS infrastructure configuration for the `us-east-1` region.

## Terraform state

The `apply-<env>.sh` script uses the `state/<env>` folder to store state using git. It is not setup to use a remote state store.

## Data Sources

Data Source files are file named in the format `data.<resource>.<name>.tf`

## Variables

Variables are grouped in files in the format `variables.<feature name>.<service>.<name>.tf`

## Resources

Resources are grouped in files in the format `resources.<feature name>.<service>.<resource>.<name>.tf`
