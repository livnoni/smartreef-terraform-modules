#!/bin/bash
clear

# when this file is updated, corresponding updates are required in `destroy-sandbox.sh`

# env vars
export TF_VAR_awsProfile=""
export TF_VAR_awsAccount=""
export TF_VAR_awsRegion="ap-southeast-2"

# ssh-keygen
export TF_VAR_ec2KeyPairDataMongoDbPublicKeyPath=""
export TF_VAR_ec2KeyPairDataMySqlPublicKeyPath=""

# apply with the appropriate state
terraform apply -state=state/sandbox/terraform.tfstate