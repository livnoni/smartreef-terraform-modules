provider "aws" {
  region  = "${var.awsRegion}"
  profile = "${var.awsProfile}"
}
